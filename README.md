# laravel5Angularjs

## Frontend

* Angularjs
* Bootstrap3
* Sass 
* Task Manager -> Gulp.

## Backend

* PHP Laravel 5
* Database -> MySql.

## Installation

1. **npm install**
2. **Go to Runner folder** and install dependencies with **npm** (npm install)

## Run Frontend

1. Open cmd inside **/Runner**  and copy this command line: 

 * Dev Mode
 ```
 gulp run serve:dev
 ```
 * Prod Mode
 ```
 gulp run serve:dist
 ```

## Build Frontend

1. Copy Assets files to dist folder
 ```
gulp run copyFiles:Dist
 ```
2. Minify js and paste it into dist folder.

```
gulp run getReady:Dist
```


